Some initial tests on the effect on finite-width WPs expressed through QM/MM MD Trajectories on  Debye Scattering.
==================================================================================================================

See `WP_Debye.ipynb`

Requires the packages [ASE](https://wiki.fysik.dtu.dk/ase/) and [CMMTools](https://gitlab.com/asod/cmmtools). 

If you wanna see molecules in your notebook you also need [nglview](https://github.com/nglviewer/nglview#installation). 


The npy files containing the data can be obtained [here](https://www.dropbox.com/s/3lrmvqwhlzw7rg5/npys.zip?dl=0)
